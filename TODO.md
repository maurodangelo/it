# TODOLIST

## Général
* Choix de la licence pour les codes sources (probablement BSD-3)
* Choix de la licence pour les documents (probablement une des CC https://creativecommons.org/licenses/?lang=fr-FR)
* lister les contributeurs dans `CONTRIBUTORS.md`
* ajouter des references Wikipedia dans le glossaire

## Tutoriel
* Guide d'installation Windows 10
* Tutoriel pour [Fritzing](https://fritzing.org/home/)
* Tutoriel pour [Jupyter](https://jupyter.org/)
* Tutoriel pour Matlab
* Tutoriel pour les cartes fille : MEMS, Teseo, DC Motor Driver, Stepper Driver ...

## Pages
* Générer les favicons à partir du logo
* Ajout d'un menu pour le changement de langue
* Ajout d'une grille pour la page d'accueil (Masonry https://masonry.desandro.com/)
* Ajout les tags aux pages et faire une page de tags 
* Ajout de SEO tags https://github.com/jekyll/jekyll-seo-tag
* Ajouter le logo STM32Python
* Ajouter des permalinks dans les index des tutoriels
* Ajouter un site à propos de la vie privée (site.privacy dans _config.yml)
* Ajout du feed.xml ?

## Conventions d'écriture
* nom de fichiers et images en minuscule
* rédaction/correction des tutoriels à la seconde personne du pluriel "vous".