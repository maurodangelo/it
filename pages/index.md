---
layout: page
title: STM32Python
permalink: /
---
![Image](assets/img/Under_construction.jpg)

# Benvenuto in STM32MicroPython

<p align="center">
<img src="assets/logos/stm32upython.svg" width="400px" alt="logo"/>
</p>

## Contesto

La riforma del liceo introduce un nuovo corso seguito da tutti gli studenti della scuola secondaria e tecnologica: SNT (Scienze numeriche e tecnologie). Uno dei temi affrontati da questo corso è l' Internet of things (IoT) , trattato nel capitolo “Embedded computing e oggetti connessi”, che rappresenta l'estensione di Internet alle cose e agli luoghi del mondo fisico.

L'obiettivo è portare questi giovani a un primo livello di comprensione dell [Internet delle cose](https://it.wikipedia.org/wiki/Internet_delle_cose). La sfida è promuovere un orientamento scelto, in questo caso qui verso l'ingegneria digitale. La quota di “digitale” e “informatica” nell'insegnamento è stata notevolmente aumentata con la riforma del liceo.

## Obiettivo

L'obiettivo di STM32Python è fornire agli insegnanti delle scuole superiori e agli studenti delle scuole superiori materiali didattici open-source per l'iniziazione all [Internet delle cose](https://it.wikipedia.org/wiki/Internet_delle_cose) per l'insegnamento di SNT (Digital Sciences and Technology) . Questi supporti sono basati sulla piattaforma ST Microelectronics Nucleo STM32. Consentono di produrre assembly elettronici e programmi per microcontrollori STM32 con linguaggi C / C ++ e microPython.

I materiali prodotti possono essere utilizzati anche da altri corsi generali primi e terminali, in particolare nella specialità NSI (Numerics and Computer Sciences), nella specialità IS (Engineering Sciences), o nella serie tecnologica STI2D (Sciences and Technologies of the "Industria e sviluppo sostenibile).
Partner



## Partner
I partner di STM32Python sono:
* i rettorati delle accademie di  [Grenoble](http://www.ac-grenoble.fr) e [Aix-Marseille](http://www.ac-aix-marseille.fr),
* [ST Microelectronics](https://www.st.com),
* [Inventhys](http://www.inventhys.com),
* [Polytech Grenoble](https://www.polytech-grenoble.fr), [Grenoble INP Institut d'ingénierie et de management](https://www.grenoble-inp.fr/), [Université Grenoble Alpes](https://www.univ-grenoble-alpes.fr).
